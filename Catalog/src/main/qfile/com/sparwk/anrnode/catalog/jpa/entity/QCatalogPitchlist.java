package com.sparwk.anrnode.catalog.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCatalogPitchlist is a Querydsl query type for CatalogPitchlist
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCatalogPitchlist extends EntityPathBase<CatalogPitchlist> {

    private static final long serialVersionUID = -1338783646L;

    public static final QCatalogPitchlist catalogPitchlist = new QCatalogPitchlist("catalogPitchlist");

    public final com.sparwk.anrnode.catalog.jpa.entity.auditing.QBaseEntity _super = new com.sparwk.anrnode.catalog.jpa.entity.auditing.QBaseEntity(this);

    public final StringPath bookmarkYn = createString("bookmarkYn");

    public final NumberPath<Long> catalogId = createNumber("catalogId", Long.class);

    public final StringPath interExterStatus = createString("interExterStatus");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> pitchlistId = createNumber("pitchlistId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QCatalogPitchlist(String variable) {
        super(CatalogPitchlist.class, forVariable(variable));
    }

    public QCatalogPitchlist(Path<? extends CatalogPitchlist> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCatalogPitchlist(PathMetadata metadata) {
        super(CatalogPitchlist.class, metadata);
    }

}


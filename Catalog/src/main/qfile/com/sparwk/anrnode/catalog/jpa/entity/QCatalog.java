package com.sparwk.anrnode.catalog.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCatalog is a Querydsl query type for Catalog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCatalog extends EntityPathBase<Catalog> {

    private static final long serialVersionUID = -1175674148L;

    public static final QCatalog catalog = new QCatalog("catalog");

    public final com.sparwk.anrnode.catalog.jpa.entity.auditing.QBaseEntity _super = new com.sparwk.anrnode.catalog.jpa.entity.auditing.QBaseEntity(this);

    public final NumberPath<Long> catalogId = createNumber("catalogId", Long.class);

    public final ListPath<CatalogPitchlist, QCatalogPitchlist> catalogPitchlist = this.<CatalogPitchlist, QCatalogPitchlist>createList("catalogPitchlist", CatalogPitchlist.class, QCatalogPitchlist.class, PathInits.DIRECT2);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QCatalog(String variable) {
        super(Catalog.class, forVariable(variable));
    }

    public QCatalog(Path<? extends Catalog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCatalog(PathMetadata metadata) {
        super(Catalog.class, metadata);
    }

}


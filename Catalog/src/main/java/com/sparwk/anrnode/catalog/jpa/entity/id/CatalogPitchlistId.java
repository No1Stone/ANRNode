package com.sparwk.anrnode.catalog.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class CatalogPitchlistId implements Serializable {

    private Long catalogId;
    private Long pitchlistId;

}

package com.sparwk.anrnode.catalog.jpa.entity;

import com.sparwk.anrnode.catalog.jpa.entity.auditing.BaseEntity;
import com.sparwk.anrnode.catalog.jpa.entity.id.CatalogPitchlistId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_catalog_pitchlist")
@IdClass(CatalogPitchlistId.class)
public class CatalogPitchlist extends BaseEntity {

    @Id
    @Column(name = "catalog_id")
    private Long catalogId;
    @Id
    @Column(name = "pitchlist_id")
    private Long pitchlistId;
    @Column(name = "bookmark_yn", nullable = true)
    private String bookmarkYn;
    @Column(name = "inter_exter_status", nullable = true)
    private String interExterStatus;

    @Builder
    CatalogPitchlist(
            Long catalogId,
            Long pitchlistId,
            String bookmarkYn,
            String interExterStatus
            ) {
        this.catalogId=catalogId;
        this.pitchlistId=pitchlistId;
        this.bookmarkYn=bookmarkYn;
        this.interExterStatus=interExterStatus;
    }


}

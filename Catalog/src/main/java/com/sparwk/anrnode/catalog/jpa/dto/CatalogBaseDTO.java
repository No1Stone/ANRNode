package com.sparwk.anrnode.catalog.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CatalogBaseDTO {

    private Long catalogId;
    private Long profileId;

}

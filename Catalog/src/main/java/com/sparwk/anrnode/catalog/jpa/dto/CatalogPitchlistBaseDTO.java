package com.sparwk.anrnode.catalog.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CatalogPitchlistBaseDTO {
    private Long catalogId;
    private Long pitchlistId;
    private String bookmarkYn;
    private String interExterStatus;
}

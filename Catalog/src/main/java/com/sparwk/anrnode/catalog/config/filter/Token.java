package com.sparwk.anrnode.catalog.config.filter;

import lombok.*;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class Token {

    private UserInfoDTO userInfoDTO;
}

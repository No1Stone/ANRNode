package com.sparwk.anrnode.catalog.biz.v1.catalog;

import com.sparwk.anrnode.catalog.biz.v1.catalog.dto.CatalogPitchlistRequest;
import com.sparwk.anrnode.catalog.biz.v1.catalog.dto.CatalogRequest;
import com.sparwk.anrnode.catalog.jpa.dto.CatalogBaseDTO;
import com.sparwk.anrnode.catalog.jpa.dto.CatalogPitchlistBaseDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/catalog")
@CrossOrigin("*")
@Api(tags = "catalog Server")
public class CatalogController {

    @Autowired
    private CatalogService catalogService;

    private final Logger logger = LoggerFactory.getLogger(CatalogController.class);

    @PostMapping(path = "/info")
    public ResponseEntity<CatalogBaseDTO> CatalogSaveController(
            @Valid @RequestBody CatalogRequest DTO) {
        CatalogBaseDTO result = catalogService.CatalogSaveService(DTO);
        return ResponseEntity.ok(result);
    }

    @PostMapping(path = "/pitchlist/info")
    public ResponseEntity<CatalogPitchlistBaseDTO> CatalogPitchlistSaveController(
            @Valid @RequestBody CatalogPitchlistRequest DTO) {
        CatalogPitchlistBaseDTO
                result = catalogService.CatalogPitchlistSaveService(DTO);
        return ResponseEntity.ok(result);
    }


    @GetMapping(path = "/info/{catalogId}")
    public ResponseEntity<List<CatalogBaseDTO>> CatalogSelect(
            @PathVariable(name = "catalogId") Long catalogId, HttpServletRequest req) {
        List<CatalogBaseDTO> result = catalogService.CatalogSelectService(catalogId);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/pitchlist/{catalogId}")
    public ResponseEntity<List<CatalogPitchlistBaseDTO>> CatalogPitchlistSelect(
            @PathVariable(name = "catalogId") Long catalogId, HttpServletRequest req) {
        List<CatalogPitchlistBaseDTO> result = catalogService.CatalogPitchlistSelectService(catalogId);
        return ResponseEntity.ok(result);
    }


}

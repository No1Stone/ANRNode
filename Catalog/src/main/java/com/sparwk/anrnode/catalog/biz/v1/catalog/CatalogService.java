package com.sparwk.anrnode.catalog.biz.v1.catalog;

import com.sparwk.anrnode.catalog.biz.v1.catalog.dto.CatalogPitchlistRequest;
import com.sparwk.anrnode.catalog.biz.v1.catalog.dto.CatalogRequest;
import com.sparwk.anrnode.catalog.jpa.dto.CatalogBaseDTO;
import com.sparwk.anrnode.catalog.jpa.dto.CatalogPitchlistBaseDTO;
import com.sparwk.anrnode.catalog.jpa.repository.CatalogBaseServiceRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {

    //    @Autowired
//    private CatalogRepository catalogRepository;
//    @Autowired
//    private CatalogPitchlistRepository catalogPitchlistRepository;
    @Autowired
    private CatalogBaseServiceRepository catalogBaseServiceRepository;
    @Autowired
    private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(CatalogService.class);


    public CatalogBaseDTO CatalogSaveService(CatalogRequest dto) {
        CatalogBaseDTO result = catalogBaseServiceRepository
                .CatalogSaveService(modelMapper.map(dto, CatalogBaseDTO.class));
        return result;
    }

    public CatalogPitchlistBaseDTO CatalogPitchlistSaveService(CatalogPitchlistRequest dto) {
        CatalogPitchlistBaseDTO result = catalogBaseServiceRepository
                .CatalogPitchlistSaveService(modelMapper.map(dto, CatalogPitchlistBaseDTO.class));
        return result;
    }

    public List<CatalogBaseDTO> CatalogSelectService(Long catalogId) {

        List<CatalogBaseDTO> result = catalogBaseServiceRepository.CatalogSelectService(catalogId);
        return result;
    }

    public List<CatalogPitchlistBaseDTO> CatalogPitchlistSelectService(Long catalogId) {
        List<CatalogPitchlistBaseDTO> result = catalogBaseServiceRepository.CatalogPitchlistSelectService(catalogId);
        return result;
    }

}

package com.sparwk.anrnode.catalog.jpa.entity;

import com.sparwk.anrnode.catalog.jpa.entity.auditing.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_catalog")
public class Catalog extends BaseEntity {

    @Id
    @GeneratedValue(generator = "tb_catalog_seq")
    @Column(name = "catalog_id")
    private Long catalogId;
    @Column(name = "profile_id")
    private Long profileId;

    @Builder
    Catalog(
            Long catalogId,
            Long profileId
    ) {
        this.catalogId = catalogId;
        this.profileId = profileId;
    }


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = CatalogPitchlist.class)
    @JoinColumn(name = "catalog_id", referencedColumnName = "catalog_id")
    private List<CatalogPitchlist> catalogPitchlist;


}

package com.sparwk.anrnode.catalog.biz.v1.catalog.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CatalogPitchlistRequest {
    private Long catalogId;
    private Long pitchlistId;
    private String bookmarkYn;
    private String interExterStatus;
}

package com.sparwk.anrnode.catalog.jpa.repository;

import com.sparwk.anrnode.catalog.jpa.dto.CatalogBaseDTO;
import com.sparwk.anrnode.catalog.jpa.dto.CatalogPitchlistBaseDTO;
import com.sparwk.anrnode.catalog.jpa.entity.Catalog;
import com.sparwk.anrnode.catalog.jpa.entity.CatalogPitchlist;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CatalogBaseServiceRepository {

    private final Logger logger = LoggerFactory.getLogger(CatalogBaseServiceRepository.class);
    @Autowired
    private CatalogPitchlistRepository catalogPitchlistRepository;
    @Autowired
    private CatalogRepository catalogRepository;
    @Autowired
    private ModelMapper modelMapper;

    public CatalogBaseDTO CatalogSaveService(CatalogBaseDTO DTO) {
        Catalog entity = modelMapper.map(DTO, Catalog.class);
        catalogRepository.save(entity);
        CatalogBaseDTO result = modelMapper.map(entity, CatalogBaseDTO.class);
        return result;
    }

    public CatalogPitchlistBaseDTO CatalogPitchlistSaveService(CatalogPitchlistBaseDTO DTO) {
        CatalogPitchlist entity = modelMapper.map(DTO, CatalogPitchlist.class);
        catalogPitchlistRepository.save(entity);
        CatalogPitchlistBaseDTO result = modelMapper.map(entity, CatalogPitchlistBaseDTO.class);
        return result;
    }



    public List<CatalogBaseDTO> CatalogSelectService(Long catalogId) {
        return catalogRepository.findById(catalogId)
                .stream().map(e -> modelMapper.map(e, CatalogBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<CatalogPitchlistBaseDTO> CatalogPitchlistSelectService(Long catalogId) {
        return catalogPitchlistRepository.findByCatalogId(catalogId)
                .stream().map(e -> modelMapper.map(e, CatalogPitchlistBaseDTO.class))
                .collect(Collectors.toList());
    }

}

package com.sparwk.anrnode.catalog.biz.v1.catalog.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CatalogRequest {

    private Long catalogId;
    private Long profileId;

}

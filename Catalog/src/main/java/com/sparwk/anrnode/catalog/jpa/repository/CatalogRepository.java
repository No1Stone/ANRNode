package com.sparwk.anrnode.catalog.jpa.repository;

import com.sparwk.anrnode.catalog.jpa.entity.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CatalogRepository extends JpaRepository<Catalog, Long> {
}

package com.sparwk.anrnode.catalog.jpa.repository;

import com.sparwk.anrnode.catalog.jpa.entity.CatalogPitchlist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CatalogPitchlistRepository extends JpaRepository<CatalogPitchlist, Long> {

    List<CatalogPitchlist> findByCatalogId(Long catalogId);

}

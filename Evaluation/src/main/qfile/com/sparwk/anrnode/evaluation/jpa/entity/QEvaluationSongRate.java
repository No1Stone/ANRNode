package com.sparwk.anrnode.evaluation.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEvaluationSongRate is a Querydsl query type for EvaluationSongRate
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEvaluationSongRate extends EntityPathBase<EvaluationSongRate> {

    private static final long serialVersionUID = 143162493L;

    public static final QEvaluationSongRate evaluationSongRate = new QEvaluationSongRate("evaluationSongRate");

    public final com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity _super = new com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity(this);

    public final NumberPath<Long> evalAnrId = createNumber("evalAnrId", Long.class);

    public final NumberPath<Long> evalId = createNumber("evalId", Long.class);

    public final NumberPath<Long> evalRate = createNumber("evalRate", Long.class);

    public final StringPath evalTemplDtlCd = createString("evalTemplDtlCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QEvaluationSongRate(String variable) {
        super(EvaluationSongRate.class, forVariable(variable));
    }

    public QEvaluationSongRate(Path<? extends EvaluationSongRate> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEvaluationSongRate(PathMetadata metadata) {
        super(EvaluationSongRate.class, metadata);
    }

}


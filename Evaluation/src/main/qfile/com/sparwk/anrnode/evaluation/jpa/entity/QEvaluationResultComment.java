package com.sparwk.anrnode.evaluation.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEvaluationResultComment is a Querydsl query type for EvaluationResultComment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEvaluationResultComment extends EntityPathBase<EvaluationResultComment> {

    private static final long serialVersionUID = 1066847130L;

    public static final QEvaluationResultComment evaluationResultComment = new QEvaluationResultComment("evaluationResultComment");

    public final com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity _super = new com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity(this);

    public final NumberPath<Long> evalId = createNumber("evalId", Long.class);

    public final NumberPath<Long> evalOwnerId = createNumber("evalOwnerId", Long.class);

    public final NumberPath<Long> evalRate = createNumber("evalRate", Long.class);

    public final StringPath evalRateCommt = createString("evalRateCommt");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QEvaluationResultComment(String variable) {
        super(EvaluationResultComment.class, forVariable(variable));
    }

    public QEvaluationResultComment(Path<? extends EvaluationResultComment> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEvaluationResultComment(PathMetadata metadata) {
        super(EvaluationResultComment.class, metadata);
    }

}


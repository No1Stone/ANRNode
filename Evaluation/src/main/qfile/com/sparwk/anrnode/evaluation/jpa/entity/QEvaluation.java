package com.sparwk.anrnode.evaluation.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEvaluation is a Querydsl query type for Evaluation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEvaluation extends EntityPathBase<Evaluation> {

    private static final long serialVersionUID = 179387592L;

    public static final QEvaluation evaluation = new QEvaluation("evaluation");

    public final com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity _super = new com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity(this);

    public final DateTimePath<java.time.LocalDateTime> evalEndDt = createDateTime("evalEndDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> evalId = createNumber("evalId", Long.class);

    public final NumberPath<Long> evalOwnerId = createNumber("evalOwnerId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> evalStartDt = createDateTime("evalStartDt", java.time.LocalDateTime.class);

    public final StringPath evalTemplCd = createString("evalTemplCd");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    public final NumberPath<Long> pitchId = createNumber("pitchId", Long.class);

    public final StringPath reEvalYn = createString("reEvalYn");

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public final NumberPath<Long> songId = createNumber("songId", Long.class);

    public QEvaluation(String variable) {
        super(Evaluation.class, forVariable(variable));
    }

    public QEvaluation(Path<? extends Evaluation> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEvaluation(PathMetadata metadata) {
        super(Evaluation.class, metadata);
    }

}


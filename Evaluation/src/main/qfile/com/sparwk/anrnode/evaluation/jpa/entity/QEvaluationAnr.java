package com.sparwk.anrnode.evaluation.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEvaluationAnr is a Querydsl query type for EvaluationAnr
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEvaluationAnr extends EntityPathBase<EvaluationAnr> {

    private static final long serialVersionUID = 1196503037L;

    public static final QEvaluationAnr evaluationAnr = new QEvaluationAnr("evaluationAnr");

    public final com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity _super = new com.sparwk.anrnode.evaluation.jpa.entity.auditing.QBaseEntity(this);

    public final NumberPath<Long> evalAnrId = createNumber("evalAnrId", Long.class);

    public final NumberPath<Long> evalId = createNumber("evalId", Long.class);

    //inherited
    public final DateTimePath<java.time.LocalDateTime> modDt = _super.modDt;

    //inherited
    public final NumberPath<Long> modUsr = _super.modUsr;

    //inherited
    public final DateTimePath<java.time.LocalDateTime> regDt = _super.regDt;

    //inherited
    public final NumberPath<Long> regUsr = _super.regUsr;

    public QEvaluationAnr(String variable) {
        super(EvaluationAnr.class, forVariable(variable));
    }

    public QEvaluationAnr(Path<? extends EvaluationAnr> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEvaluationAnr(PathMetadata metadata) {
        super(EvaluationAnr.class, metadata);
    }

}


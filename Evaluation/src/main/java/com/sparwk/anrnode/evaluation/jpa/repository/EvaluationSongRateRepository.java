package com.sparwk.anrnode.evaluation.jpa.repository;

import com.sparwk.anrnode.evaluation.jpa.entity.EvaluationSongRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvaluationSongRateRepository extends JpaRepository<EvaluationSongRate, Long> {
}

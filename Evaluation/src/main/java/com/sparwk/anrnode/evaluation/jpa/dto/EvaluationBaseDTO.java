package com.sparwk.anrnode.evaluation.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EvaluationBaseDTO {
    private Long evalId;
    private Long songId;
    private Long pitchId;
    private Long evalOwnerId;
    private LocalDateTime evalStartDt;
    private LocalDateTime evalEndDt;
    private String evalTemplCd;
    private String reEvalYn;
}

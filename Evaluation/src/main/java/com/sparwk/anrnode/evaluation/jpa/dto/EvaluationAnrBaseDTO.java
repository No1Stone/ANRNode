package com.sparwk.anrnode.evaluation.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EvaluationAnrBaseDTO {
    private Long evalId;
    private Long evalAnrId;
}

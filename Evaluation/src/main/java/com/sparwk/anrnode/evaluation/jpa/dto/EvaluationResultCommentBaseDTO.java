package com.sparwk.anrnode.evaluation.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EvaluationResultCommentBaseDTO {

    private Long evalId ;
    private Long songId ;
    private Long evalOwnerId ;
    private Long evalRate ;
    private String evalRateCommt ;

}

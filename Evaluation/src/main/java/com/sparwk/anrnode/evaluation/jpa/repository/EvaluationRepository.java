package com.sparwk.anrnode.evaluation.jpa.repository;

import com.sparwk.anrnode.evaluation.jpa.entity.Evaluation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvaluationRepository extends JpaRepository<Evaluation, Long> {
}

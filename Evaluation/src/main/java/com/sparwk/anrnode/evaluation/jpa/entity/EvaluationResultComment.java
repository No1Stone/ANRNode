package com.sparwk.anrnode.evaluation.jpa.entity;

import com.sparwk.anrnode.evaluation.jpa.entity.auditing.BaseEntity;
import com.sparwk.anrnode.evaluation.jpa.entity.id.EvaluationResultCommentId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_evaluation_result_comment")
@IdClass(EvaluationResultCommentId.class)
public class EvaluationResultComment extends BaseEntity {

    @Id
    @Column(name = "eval_id ", nullable = true)
    private Long evalId;
    @Id
    @Column(name = "song_id ", nullable = true)
    private Long songId;
    @Column(name = "eval_owner_id ", nullable = true)
    private Long evalOwnerId;
    @Column(name = "eval_rate ", nullable = true)
    private Long evalRate;
    @Column(name = "eval_rate_commt ", nullable = true)
    private String evalRateCommt;


    @Builder
    EvaluationResultComment(
            Long evalId,
            Long songId,
            Long evalOwnerId,
            Long evalRate,
            String evalRateCommt

    ) {
        this.evalId = evalId;
        this.songId = songId;
        this.evalOwnerId = evalOwnerId;
        this.evalRate = evalRate;
        this.evalRateCommt = evalRateCommt;

    }

}


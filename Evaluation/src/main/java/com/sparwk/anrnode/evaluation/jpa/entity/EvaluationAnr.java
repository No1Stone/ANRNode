package com.sparwk.anrnode.evaluation.jpa.entity;

import com.sparwk.anrnode.evaluation.jpa.entity.auditing.BaseEntity;
import com.sparwk.anrnode.evaluation.jpa.entity.id.EvaluationAnrId;
import lombok.*;

import javax.persistence.*;


@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_evaluation_anr")
@IdClass(EvaluationAnrId.class)
public class EvaluationAnr extends BaseEntity {

    @Id
    @Column(name = "eval_id", nullable = true)
    private Long evalId;

    @Id
    @Column(name = "eval_anr_id", nullable = true)
    private Long evalAnrId;

    @Builder
    EvaluationAnr(
            Long evalId,
            Long evalAnrId
    ) {
        this.evalId = evalId;
        this.evalAnrId = evalAnrId;
    }

}

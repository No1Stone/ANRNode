package com.sparwk.anrnode.evaluation.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class EvaluationResultCommentId implements Serializable {

    private Long evalId;
    private Long songId;

}

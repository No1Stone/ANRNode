package com.sparwk.anrnode.evaluation.jpa.entity;


import com.sparwk.anrnode.evaluation.jpa.entity.auditing.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_evaluation")
public class Evaluation extends BaseEntity {

    @Id
    @Column(name = "eval_id")
    private Long evalId;
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Column(name = "pitch_id", nullable = true)
    private Long pitchId;
    @Column(name = "eval_owner_id", nullable = true)
    private Long evalOwnerId;
    @Column(name = "eval_start_dt", nullable = true)
    private LocalDateTime evalStartDt;
    @Column(name = "eval_end_dt", nullable = true)
    private LocalDateTime evalEndDt;
    @Column(name = "eval_templ_cd", nullable = true)
    private String evalTemplCd;
    @Column(name = "re_eval_yn", nullable = true)
    private String reEvalYn;

    @Builder
    Evaluation(
            Long evalId,
            Long songId,
            Long pitchId,
            Long evalOwnerId,
            LocalDateTime evalStartDt,
            LocalDateTime evalEndDt,
            String evalTemplCd,
            String reEvalYn
    ) {
        this.evalId = evalId;
        this.songId = songId;
        this.pitchId = pitchId;
        this.evalOwnerId = evalOwnerId;
        this.evalStartDt = evalStartDt;
        this.evalEndDt = evalEndDt;
        this.evalTemplCd = evalTemplCd;
        this.reEvalYn = reEvalYn;
    }

}

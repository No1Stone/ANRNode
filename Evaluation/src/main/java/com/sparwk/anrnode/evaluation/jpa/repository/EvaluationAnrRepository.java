package com.sparwk.anrnode.evaluation.jpa.repository;

import com.sparwk.anrnode.evaluation.jpa.entity.EvaluationAnr;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvaluationAnrRepository extends JpaRepository<EvaluationAnr, Long> {
}

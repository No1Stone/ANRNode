package com.sparwk.anrnode.evaluation.config.filter;

import com.sparwk.anrnode.evaluation.config.common.ClientIp;
import com.sparwk.anrnode.evaluation.config.common.ZoneUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Intercepteradapter implements HandlerInterceptor {

    @Autowired
    private ClientIp clientIp;
    @Autowired
    private TokenDecode tokenDecode;
    @Autowired
    private ModelMapper modelMapper;

    private final Logger logger = LoggerFactory.getLogger(Intercepteradapter.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
//
//        if (request.getMethod().equals("OPTIONS")) {
//            //CORS인증 유효 처리를 위해
//            return true;
//        }

        String authorization =request.getHeader("Authorization");
        logger.info("intercepter token check - {}",authorization);
        String token = authorization.substring(7);
        logger.info(token);
        String detoken = tokenDecode.getSubject(token);
        logger.info(detoken);
        String delUserInfo = detoken.substring(12);
        logger.info(delUserInfo);
        String[] tokenSpl1 = delUserInfo.split(",");
        logger.info(tokenSpl1[0]);
        String[] tokenSpl2 = tokenSpl1[0].split("=");
        logger.info(tokenSpl2[0]);
        logger.info(tokenSpl2[1]);
        long accountId = Long.parseLong(tokenSpl2[1]);

//        Object aa = jp.parse(detoken);
//        JSONArray bb = (JSONArray) aa;
//        UserInfoDTO bb = (UserInfoDTO) detoken;
//        logger.info("aaaaaaaaaaaaa - {}",  bb.getAccountId());

//        aa.getAccountId() = obj.get

//        UserInfoDTO aa = gson.fromJson(detoken, UserInfoDTO.class);

        /*
//        String token = Authorization.substring(7);
//        HttpHeaders headers = new HttpHeaders();
//        headers.set("Authorization", Authorization);
//        UriComponentsBuilder builder =
//                UriComponentsBuilder
//                        .newInstance()
//                        .scheme("http")
//                        .host("personalization.sparwkdev.com")
//                        .path("/auth/V1/token/getUserInfo");
//        UriComponents uriComponents = builder.build();
//
//
//        HttpEntity<String> entity = new HttpEntity<>(headers);
//        ResponseEntity<String> tokenResponse = restTemplate
//                .exchange(uriComponents.toUriString(), HttpMethod.GET, entity,String.class);
//        JSONObject obj = new JSONObject(tokenResponse.getBody());
//        logger.info("object value - {}",obj.toString());
//
//        Gson gson = new Gson();
//
//        UserInfoResponseBaseDTO userInfo = gson.fromJson(obj.toString(), UserInfoResponseBaseDTO.class);
//        logger.info("userinfo  - {}", userInfo);
*/

        TokenBodyDTO tb = new TokenBodyDTO();
        tb.setAccountId(accountId);
        //logger.info("intercepter pre");
        //logger.info("header - {}", header);
        //logger.info("body - {}", body);
        //logger.info("Signature - {}", Signature);
        //logger.info("tb info - {}",tb);
        //logger.info("tb info - {}",tb.getSub());
        tb.setUserIp(clientIp.getClientIp(request));
        tb.setUtc0(ZoneUtils.UTC0());
        //유저 토큰 utc terr 정보를 넣어서 셋
        //tb.setUserIp(ZoneUtils.cumstum());
        request.setAttribute("TokenBody", tb);
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

}

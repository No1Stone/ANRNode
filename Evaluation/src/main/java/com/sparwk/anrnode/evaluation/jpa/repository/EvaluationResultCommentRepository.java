package com.sparwk.anrnode.evaluation.jpa.repository;

import com.sparwk.anrnode.evaluation.jpa.entity.EvaluationResultComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvaluationResultCommentRepository extends JpaRepository<EvaluationResultComment, Long> {
}

package com.sparwk.anrnode.evaluation.config.filter;

import lombok.*;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class Token {

    private UserInfoDTO userInfoDTO;
}

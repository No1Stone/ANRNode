package com.sparwk.anrnode.evaluation.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EvaluationSongRateBaseDTO {

    private Long evalId;
    private Long evalAnrId;
    private Long songId;
    private String evalTemplDtlCd;
    private Long evalRate;

}

package com.sparwk.anrnode.evaluation.jpa.entity;

import com.sparwk.anrnode.evaluation.jpa.entity.auditing.BaseEntity;
import com.sparwk.anrnode.evaluation.jpa.entity.id.EvaluationSongRateId;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_evaluation_song_rate")
@IdClass(EvaluationSongRateId.class)
public class EvaluationSongRate extends BaseEntity {

    @Id
    @Column(name = "eval_id", nullable = true)
    private Long evalId;
    @Id
    @Column(name = "eval_anr_id", nullable = true)
    private Long evalAnrId;
    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "eval_templ_dtl_cd", nullable = true)
    private String evalTemplDtlCd;
    @Column(name = "eval_rate", nullable = true)
    private Long evalRate;

    @Builder
    EvaluationSongRate(
            Long evalId,
            Long evalAnrId,
            Long songId,
            String evalTemplDtlCd,
            Long evalRate
    ) {
        this.evalId = evalId;
        this.evalAnrId = evalAnrId;
        this.songId = songId;
        this.evalTemplDtlCd = evalTemplDtlCd;
        this.evalRate = evalRate;
    }

}
